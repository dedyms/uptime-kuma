FROM registry.gitlab.com/dedyms/apprise:latest AS apprise

FROM registry.gitlab.com/dedyms/node:14-dev AS fetcher
ARG TARGETARCH
WORKDIR $HOME
RUN apt update && apt install -y --no-install-recommends python3 python-is-python3 libsqlite3-dev && git clone --depth=1 https://github.com/louislam/uptime-kuma.git uptime-kuma && npm -g install pnpm

#RUN if [ "${TARGETARCH}" != "amd64" ]; then \
#        apt install -y chromium; \
#    fi;
WORKDIR $HOME/uptime-kuma
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
RUN npm install && \
    npm run build && \
    npm prune --prod

#WORKDIR $HOME/uptime-kuma
#RUN npm install && \
#    npm run build

FROM registry.gitlab.com/dedyms/node:14
RUN apt update && apt install -y --no-install-recommends \
    python3-minimal libffi7 libssl1.1 python3-setuptools iputils-ping && \
    apt clean && \
    rm -rf /var/lib/apt/* && npm install -g pnpm
COPY --from=apprise --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/.local $HOME/.local
COPY --from=fetcher --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/uptime-kuma $HOME/uptime-kuma
WORKDIR $HOME/uptime-kuma
VOLUME $HOME/uptime-kuma/data
EXPOSE 3001
HEALTHCHECK --interval=60s --timeout=30s --start-period=300s CMD node extra/healthcheck.js
USER $CONTAINERUSER
CMD ["pnpm", "run", "start-server"]
